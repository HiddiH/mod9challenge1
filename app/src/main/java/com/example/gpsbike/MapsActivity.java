package com.example.gpsbike;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Polyline line;
    private GPSBikeAPIInterface gpsBikeAPIInterface;

    List<LatLng> lineCoord;
    private boolean isChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        gpsBikeAPIInterface = new GPSBikeAPIInterface(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        dataRefresh();
    }

    void showSnack(String snackContent) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar mySnackbar = Snackbar.make(parentLayout, snackContent, Snackbar.LENGTH_SHORT);
        mySnackbar.show();
    }

    void drawPolyline() {

    }

    void getPolyline() {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(path, "locations.csv");
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    void dataRefresh() {
        mMap.clear();
        gpsBikeAPIInterface.getAllBumpLevels((data)->{
            LatLng marker = new LatLng(0, 0);
            for (int i = 0; i < data.length; i++) {
                marker = new LatLng(data[i].getLat(), data[i].getLon());
                double score = data[i].getScore();
                float hsv = 0;
                if (score < 10000) hsv = 120;
                else if (score < 11000) hsv = 105;
                else if (score < 12000) hsv = 90;
                else if (score < 13000) hsv = 75;
                else if (score < 14000) hsv = 60;
                else if (score < 15000) hsv = 45;
                else if (score < 16000) hsv = 30;
                else if (score < 17000) hsv = 15;
                else if (score < 18000) hsv = 0;
                mMap.addMarker(new MarkerOptions().position(marker)
                        .title("Score: " + score)
                            .icon(BitmapDescriptorFactory.fromBitmap(bitmapHueAdjust(score))));
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLng(marker));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
        });
        showSnack("Data Refreshed");
    }

    protected int averageBitmapColor(Bitmap bitmap) {
        int redBucket = 0;
        int greenBucket = 0;
        int blueBucket = 0;
        int alphaBucket = 0;
        int pixelCount = 0;

        for (int y = 0; y < bitmap.getHeight(); y++)
        {
            for (int x = 0; x < bitmap.getWidth(); x++)
            {
                int c = bitmap.getPixel(x, y);

                pixelCount++;
                redBucket += Color.red(c);
                greenBucket += Color.green(c);
                blueBucket += Color.blue(c);
                alphaBucket += Color.alpha(c);
            }
        }

        int averageColor = Color.argb((alphaBucket/pixelCount), (redBucket/pixelCount),(greenBucket / pixelCount),
                (blueBucket / pixelCount));

        return averageColor;
    }

    protected static float cleanValue(float p_val, float p_limit) {
        return Math.min(p_limit, Math.max(-p_limit, p_val));
    }

    public static ColorMatrix adjustHue(float value) {
        value = cleanValue(value, 180f) / 180f * (float) Math.PI;

        float cosVal = (float) Math.cos(value);
        float sinVal = (float) Math.sin(value);
        float lumR = 0.213f;
        float lumG = 0.715f;
        float lumB = 0.072f;
        float[] mat = new float[]
                {
                        lumR + cosVal * (1 - lumR) + sinVal * (-lumR), lumG + cosVal * (-lumG) + sinVal * (-lumG), lumB + cosVal * (-lumB) + sinVal * (1 - lumB), 0, 0,
                        lumR + cosVal * (-lumR) + sinVal * (0.143f), lumG + cosVal * (1 - lumG) + sinVal * (0.140f), lumB + cosVal * (-lumB) + sinVal * (-0.283f), 0, 0,
                        lumR + cosVal * (-lumR) + sinVal * (-(1 - lumR)), lumG + cosVal * (-lumG) + sinVal * (lumG), lumB + cosVal * (1 - lumB) + sinVal * (lumB), 0, 0,
                        0f, 0f, 0f, 1f, 0f,
                        0f, 0f, 0f, 0f, 1f };
        ColorMatrix cm = new ColorMatrix(mat);
        return cm;
    }

    public Bitmap bitmapHueAdjust(double score) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap origBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.map_marker, options);
        Bitmap drawableBitmap = origBitmap.copy(Bitmap.Config.ARGB_8888, true);
        int averageColor = averageBitmapColor(origBitmap);
        float[] hsv1 = new float[3];
        Color.RGBToHSV(Color.red(averageColor), Color.green(averageColor), Color.blue(averageColor), hsv1);
        showSnack(hsv1[0] + "");
        float targetHue;
        if (score < 10000) targetHue = 120;
        else if (score < 11000) targetHue = 105;
        else if (score < 12000) targetHue = 90;
        else if (score < 13000) targetHue = 75;
        else if (score < 14000) targetHue = 60;
        else if (score < 15000) targetHue = 45;
        else if (score < 16000) targetHue = 30;
        else if (score < 17000) targetHue = 15;
        else targetHue = 0;

        Bitmap resultBitmap = drawableBitmap;
        ColorMatrix cm = adjustHue(targetHue - hsv1[0]);
        float value = 100;
        float x = 1+((value > 0) ? 3 * value / 100 : value / 100);
        float lumR = 0.3086f;
        float lumG = 0.6094f;
        float lumB = 0.0820f;

        float[] mat = new float[]
                {
                        lumR*(1-x)+x,lumG*(1-x),lumB*(1-x),0,0,
                        lumR*(1-x),lumG*(1-x)+x,lumB*(1-x),0,0,
                        lumR*(1-x),lumG*(1-x),lumB*(1-x)+x,0,0,
                        0,0,0,1,0,
                        0,0,0,0,1
                };
        cm.postConcat(new ColorMatrix(mat));
        Canvas canvas = new Canvas(resultBitmap);
        Paint paint = new Paint();
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(filter);
        canvas.drawBitmap(origBitmap, 0,0, paint);
        return resultBitmap;
    }

    void polylineSwitch(boolean switchState) {
        if (switchState) {
            // drawPolyline();
            showSnack("Drawing polyline");
        }
        else {
            // line.remove();
            showSnack("Removing polyline");
        }
    }

    void satelliteSwitch(boolean switchState) {
        if (switchState) {
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            showSnack("Enabling satellite view");
        }
        else {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            showSnack("Returning to normal view");
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem checkable = menu.findItem(R.id.app_bar_switch_trace);
        checkable.setChecked(isChecked);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Refresh:
                dataRefresh();
                return true;
            case R.id.app_bar_switch_trace:
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                polylineSwitch(isChecked);
                return true;
            case R.id.satellite_switch:
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                satelliteSwitch(isChecked);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}