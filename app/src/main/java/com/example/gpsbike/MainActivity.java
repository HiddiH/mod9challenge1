package com.example.gpsbike;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;

import com.example.gpsbike.models.BumpLevel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

class MyLocationListener implements LocationListener {
    double lon, lat;

    double gps[][] = new double[5][2];
    long gpsTimestamp[] = new long[5];
    int gpsIndex = 0;

    long accelerometer[][];

    FileOutputStream stream;
    private GPSBikeAPIInterface gpsBikeAPIInterface;

    public MyLocationListener(GPSBikeAPIInterface gpsBikeAPIInterface, long accelerometer[][]) {
        this.gpsBikeAPIInterface = gpsBikeAPIInterface;
        this.accelerometer = accelerometer;
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        long unixTime = System.currentTimeMillis();

        File file = new File(path, "locations"+unixTime+".csv");

        try {
            stream = new FileOutputStream(file);
            stream.write("time,longitude,latitude\n".getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 5; i++)
        {
            gpsTimestamp[i] = 0;
        }
    }

    @Override
    public void onLocationChanged(Location loc) {
        lon = loc.getLongitude();
        lat = loc.getLatitude();


        try {
            long unixTime = System.currentTimeMillis();
            stream.write((unixTime + ",").getBytes());
            stream.write(Double.toString(lon).getBytes());
            stream.write(',');
            stream.write(Double.toString(lat).getBytes());
            stream.write('\n');
            gps[gpsIndex] = new double[]{lon, lat};
            gpsTimestamp[gpsIndex] = unixTime;

            if (gpsTimestamp[(gpsIndex + 3) % 5] != 0)
            {
                long meanAcceleration = 0;
                long middleTime = gpsTimestamp[(gpsIndex + 3) % 5];
                long totalWeight = 0;
                long windowTime = 1000;
                for (int i = 0; i < 1000; i++)
                {
                    if (accelerometer[1][i] != 0 && accelerometer[1][i] < (middleTime + windowTime) && accelerometer[1][i] > (middleTime - windowTime))
                    {
                        meanAcceleration += accelerometer[0][i] * abs(windowTime - (middleTime - accelerometer[1][i]));
                        totalWeight += abs(windowTime - (middleTime - accelerometer[1][i]));
                    }
                }
                if (totalWeight != 0) {
                    meanAcceleration /= totalWeight;
                    gpsBikeAPIInterface.postBumpLevel(new BumpLevel(0, lat, lon, (int) meanAcceleration), (data) -> {});
                }
            }

            gpsIndex += 1;
            gpsIndex %= 5;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor sensor;

    private GPSBikeAPIInterface gpsBikeAPIInterface;

    long accelerometer[][] = new long[2][1000];
    int accelerometerIndex = 0;
    protected LocationManager locationManager;
    protected MyLocationListener locationListener;
    double ax, ay, az;   // these are the acceleration in x,y and z axis

    FileOutputStream stream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 0);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        gpsBikeAPIInterface = new GPSBikeAPIInterface(this);
        locationListener = new MyLocationListener(gpsBikeAPIInterface, accelerometer);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        long unixTime = System.currentTimeMillis();

        File file = new File(path, "accelerometer"+unixTime+".csv");

        try {
            stream = new FileOutputStream(file);
            stream.write("time,ax,ay,az\n".getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 1000; i++)
        {
            accelerometer[1][i] = 0;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            ax=event.values[0];
            ay=event.values[1];
            az=event.values[2];
            if (stream != null) {
                try {
                    long unixTime = System.currentTimeMillis();
                    stream.write((unixTime + ",").getBytes());
                    stream.write(Double.toString(ax).getBytes());
                    stream.write(',');
                    stream.write(Double.toString(ay).getBytes());
                    stream.write(',');
                    stream.write(Double.toString(az).getBytes());
                    stream.write('\n');
                    long totalAcc = (new Double(1000 * sqrt(ax * ax + ay * ay + az * az))).longValue();
                    accelerometer[0][accelerometerIndex] = totalAcc;
                    accelerometer[1][accelerometerIndex] = unixTime;
                    accelerometerIndex += 1;
                    accelerometerIndex %= 1000;
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void openMaps(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
}