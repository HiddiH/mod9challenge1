package com.example.gpsbike;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.gpsbike.models.BumpLevel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class GPSBikeAPIInterface {
    private String baseUrl = "http://gpsbike.radohm.nl";
    private RequestQueue queue;

    public GPSBikeAPIInterface(Context ctx) {
        queue = Volley.newRequestQueue(ctx.getApplicationContext());
    }

    public interface ParseBumpLevel {
        void method(BumpLevel data);
    }

    public interface ParseBumpLevelArray {
        void method(BumpLevel[] data);
    }

    public void getAllBumpLevels(final ParseBumpLevelArray callback) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, baseUrl + "/bumplevels", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                BumpLevel[] levelData = new BumpLevel[response.length()];
                for (int i = 0; i < response.length(); ++i) {
                    try {
                        JSONObject data = response.getJSONObject(i);
                        levelData[i] = new BumpLevel(
                                (Integer)data.get("id"),
                                (Double)data.get("lat"),
                                (Double)data.get("lon"),
                                (Integer)data.get("score")
                        );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                callback.method(levelData);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Test", "something went wrong\n" + error.toString());
            }
        });

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                40000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        queue.add(jsonArrayRequest);
    }

    public void postBumpLevel(BumpLevel level, ParseBumpLevel callback) {
        JSONObject object = new JSONObject();
        try {
            object.put("lat", level.getLat());
            object.put("lon", level.getLon());
            object.put("score", level.getScore());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, baseUrl + "/bumplevels", object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    BumpLevel levelData = new BumpLevel(
                            (Integer) response.get("id"),
                            (Double) response.get("lat"),
                            (Double) response.get("lon"),
                            (Integer) response.get("score")
                    );
                    callback.method(levelData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Test", "something went wrong\n" + error.toString());
            }
        });
        queue.add(jsonObjectRequest);
    }
}
