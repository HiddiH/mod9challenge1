package com.example.gpsbike.models;

public class BumpLevel {
    private int id;
    private double lat, lon;
    private int score;

    public BumpLevel(
            int id, double lat, double lon, int score
    ) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "{" + lat + ", " + lon + "}: " + score;
    }
}